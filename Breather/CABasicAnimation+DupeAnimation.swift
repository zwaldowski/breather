//
//  UIView+DefaultAnimation.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit
import QuartzCore

extension CABasicAnimation {
    
    convenience init(action: CAAnimation, forLayer layer: CALayer!, key event: String!) {
        self.init()
        
        keyPath = event
        
        let target: AnyObject! = layer.presentationLayer() ?? layer
        fromValue = target.valueForKey(event)
        
        // CAMediaTiming
        beginTime = action.beginTime
        duration = action.duration
        speed = action.speed
        timeOffset = action.timeOffset
        repeatCount = action.repeatCount
        repeatDuration = action.repeatDuration
        autoreverses = action.autoreverses
        fillMode = action.fillMode
        
        // CAAnimation
        timingFunction = action.timingFunction
        delegate = action.delegate
    }
    
}
