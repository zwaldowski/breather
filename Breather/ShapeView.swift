//
//  ShapeView.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class ShapeView: UIView {
    
    // MARK: Init
    
    func commonInit() {
        self.shapeLayer.geometryFlipped = true
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: UIView
    
    override class func layerClass() -> AnyClass {
        return CAShapeLayer.self
    }
    
    private var shapeLayer: CAShapeLayer {
        return layer as! CAShapeLayer
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        
        if strokeColor == nil {
            shapeLayer.strokeColor = tintColor.CGColor
        }
        
        if fillColor == nil {
            shapeLayer.fillColor = tintColor.CGColor
        }
    }
    
    override func actionForLayer(layer: CALayer, forKey event: String) -> CAAction? {
        if layer == self.layer && event == "path" {
            if let transition = super.actionForLayer(layer, forKey: "backgroundColor") as? CAAnimation {
                return CABasicAnimation(action: transition, forLayer: layer, key: event)
            }
        }
        return super.actionForLayer(layer, forKey: event)
    }
    
    // MARK: Properties
    
    var path: CGPath? {
        get { return shapeLayer.path }
        set { shapeLayer.path = newValue }
    }
    
    var fillColor: UIColor? {
        didSet { shapeLayer.fillColor = fillColor.map { $0.CGColor } ?? tintColor.CGColor }
    }
    
    var usesEvenOddFillRule: Bool {
        get { return shapeLayer.fillRule == kCAFillRuleEvenOdd }
        set { shapeLayer.fillRule = newValue ? kCAFillRuleEvenOdd : kCAFillRuleNonZero }
    }
    
    var strokeColor: UIColor? {
        didSet { shapeLayer.strokeColor = strokeColor.map { $0.CGColor } ?? tintColor.CGColor }
    }
    
    var lineWidth: CGFloat {
        get { return shapeLayer.lineWidth }
        set { shapeLayer.lineWidth = newValue }
    }
    
    enum LineJoin {
        case Miter(CGFloat)
        case Round
        case Bevel
        
        init?(rawValue: String, miterLimit: CGFloat) {
            switch rawValue {
            case kCALineJoinMiter: self = .Miter(miterLimit)
            case kCALineJoinRound: self = .Round
            case kCALineJoinBevel: self = .Bevel
            default: return nil
            }
        }
        
        var rawValue: String {
            switch self {
            case Miter: return kCALineJoinMiter
            case Round: return kCALineJoinRound
            case Bevel: return kCALineJoinBevel
            }
        }
        
        var miterLimit: CGFloat {
            switch self {
            case Miter(let miter): return miter
            default: return 10
            }
        }
        
    }
    
    var lineJoin: LineJoin! {
        get {
            return LineJoin(rawValue: shapeLayer.lineJoin, miterLimit: shapeLayer.lineWidth)
        }
        set {
            shapeLayer.lineJoin = newValue.rawValue
            shapeLayer.miterLimit = newValue.miterLimit
        }
    }
    
    enum LineCap: RawRepresentable {
        case Butt
        case Round
        case Square
        
        init?(rawValue: String) {
            switch rawValue {
            case kCALineCapButt: self = .Butt
            case kCALineCapRound: self = .Round
            case kCALineCapSquare: self = .Square
            default: return nil
            }
        }
        
        var rawValue: String {
            switch self {
            case Butt: return kCALineCapButt
            case Round: return kCALineCapRound
            case Square: return kCALineCapSquare
            }
        }
        
    }
    
    var lineCap: LineCap! {
        get {
            return LineCap(rawValue: shapeLayer.lineCap)
        }
        set {
            shapeLayer.lineCap = newValue.rawValue
        }
    }
    
    var lineDash: (pattern: [CGFloat], phase: CGFloat)? {
        get {
            return (shapeLayer.lineDashPattern as! [CGFloat], shapeLayer.lineDashPhase)
        }
        set {
            if let dash = newValue {
                shapeLayer.lineDashPattern = dash.pattern as [NSNumber]
                shapeLayer.lineDashPhase = dash.phase
            } else {
                shapeLayer.lineDashPattern = nil
                shapeLayer.lineDashPhase = 0
            }
        }
    }
    
}
