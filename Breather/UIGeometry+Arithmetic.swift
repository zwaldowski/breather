//
//  UIGeometry+Arithmetic.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import CoreGraphics
import UIKit.UIGeometry

// MARK: Rounding

protocol Scalable: FloatingPointType {
    func *(lhs: Self, rhs: Self) -> Self
    func /(lhs: Self, rhs: Self) -> Self
}

extension Double: Scalable {}
extension CGFloat: Scalable {}

func rround<T: Scalable>(value: T, scale: T, function: T -> T) -> T {
    return (scale > T(1)) ? (function(value * scale) / scale) : function(value)
}

// MARK: Arithmetic

prefix func -(p: CGPoint) -> CGPoint {
    return CGPoint(x: -p.x, y: -p.y)
}

prefix func -(t: CGAffineTransform) -> CGAffineTransform {
    return CGAffineTransformInvert(t)
}

func +(lhs: CGAffineTransform, rhs: CGAffineTransform) -> CGAffineTransform {
    return CGAffineTransformConcat(lhs, rhs)
}

func -(lhs: CGAffineTransform, rhs: CGAffineTransform) -> CGAffineTransform {
    return lhs + -rhs
}

prefix func -(t: CATransform3D) -> CATransform3D {
    return CATransform3DInvert(t)
}

func +(lhs: CATransform3D, rhs: CATransform3D) -> CATransform3D {
    return CATransform3DConcat(lhs, rhs)
}

func -(lhs: CATransform3D, rhs: CATransform3D) -> CATransform3D {
    return lhs + -rhs
}

func += (inout lhs: CGAffineTransform, rhs: CGAffineTransform) { lhs = lhs + rhs }
func -= (inout lhs: CGAffineTransform, rhs: CGAffineTransform) { lhs = lhs - rhs }
func += (inout lhs: CATransform3D, rhs: CATransform3D) { lhs = lhs + rhs }
func -= (inout lhs: CATransform3D, rhs: CATransform3D) { lhs = lhs - rhs }

// MARK: Vector arithmetic

func +(lhs:CGPoint, rhs:CGPoint) -> CGPoint {
    return CGPoint(x:lhs.x + rhs.x, y:lhs.y + rhs.y)
}

func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

func *(lhs: CGPoint, rhs: CGPoint) -> CGFloat {
    return (lhs.x * rhs.x) + (lhs.y * rhs.y)
}

func +=(inout lhs: CGPoint, rhs: CGPoint) { lhs = lhs + rhs }
func -=(inout lhs: CGPoint, rhs: CGPoint) { lhs = lhs - rhs }

// MARK: Scalar arithmetic

func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

func /(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
}

func *= (inout lhs:CGPoint, rhs:CGFloat) {
    lhs = lhs * rhs
}

func /= (inout lhs:CGPoint, rhs:CGFloat) {
    lhs = lhs / rhs
}

// MARK: Equatability

func ==(lhs: CGAffineTransform, rhs: CGAffineTransform) -> Bool {
    return CGAffineTransformEqualToTransform(lhs, rhs)
}

func ==(lhs: CATransform3D, rhs: CATransform3D) -> Bool {
    return CATransform3DEqualToTransform(lhs, rhs)
}

// MARK: Trigonometry

func ...(a: CGPoint, b: CGPoint) -> CGFloat {
    let distance = a - b
    return sqrt(distance * distance)
}
