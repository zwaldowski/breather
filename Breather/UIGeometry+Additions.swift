//
//  UIGeometry+Additions.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

// MARK: Insets

extension CGRect {
    
    mutating func inset(insets: UIEdgeInsets) {
        self = UIEdgeInsetsInsetRect(self, insets)
    }
    
}

// MARK: Initializers

extension CGAffineTransform {
    
    init() {
        self = CGAffineTransformIdentity
    }
    
    init(transform: CGAffineTransform) {
        self = transform
    }
    
    init(translation tx: CGFloat, _ ty: CGFloat) {
        self = CGAffineTransformMakeTranslation(tx, ty)
    }
    
    init(scale sx: CGFloat, _ sy: CGFloat) {
        self = CGAffineTransformMakeScale(sx, sy)
    }
    
    init(angle: CGFloat) {
        self = CGAffineTransformMakeRotation(angle)
    }
    
    var isIdentity: Bool {
        return CGAffineTransformIsIdentity(self)
    }
    
}

extension CATransform3D {
    
    init() {
        self = CATransform3DIdentity
    }
    
    init(translation tx: CGFloat, _ ty: CGFloat, _ tz: CGFloat) {
        self = CATransform3DMakeTranslation(tx, ty, tz)
    }
    
    init(scale sx: CGFloat, _ sy: CGFloat, _ sz: CGFloat) {
        self = CATransform3DMakeScale(sx, sy, sz)
    }
    
    init(angle: CGFloat, x: CGFloat, y: CGFloat, z: CGFloat) {
        self = CATransform3DMakeRotation(angle, x, y, z)
    }
    
    init(affineTransform m: CGAffineTransform) {
        self = CATransform3DMakeAffineTransform(m)
    }
    
    var isIdentity: Bool {
        return CATransform3DIsIdentity(self)
    }
    
    var affineTransform: CGAffineTransform? {
        if !CATransform3DIsAffine(self) { return nil }
        return CATransform3DGetAffineTransform(self)
    }
    
}

// MARK: Transformations

extension CGAffineTransform {
    
    func translated(tx: CGFloat, _ ty: CGFloat) -> CGAffineTransform {
        return CGAffineTransformTranslate(self, tx, ty)
    }
    
    func scaled(sx: CGFloat, _ sy: CGFloat) -> CGAffineTransform {
        return CGAffineTransformScale(self, sx, sy)
    }
    
    func rotated(angle: CGFloat) -> CGAffineTransform {
        return CGAffineTransformRotate(self, angle)
    }
    
}

extension CATransform3D {
    
    func translated(tx: CGFloat, _ ty: CGFloat, _ tz: CGFloat) -> CATransform3D {
        return CATransform3DTranslate(self, tx, ty, tz)
    }
    
    func scaled(sx: CGFloat, _ sy: CGFloat, _ sz: CGFloat) -> CATransform3D {
        return CATransform3DScale(self, sx, sy, sz)
    }
    
    func rotated(angle: CGFloat, x: CGFloat, y: CGFloat, z: CGFloat) -> CATransform3D {
        return CATransform3DRotate(self, angle, x, y, z)
    }
    
}
