//
//  UIFontDescriptor+DynamicType.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

extension UIFontDescriptor {
    
    enum LetterTransform {
        case Regular
        case AllCaps
        case SmallCaps
        case PetiteCaps
        
        private var upperCaseFeature: [NSObject: AnyObject] {
            var selector: Int
            switch self {
            case .AllCaps: selector = kUpperCaseSmallCapsSelector
            default: selector = kDefaultUpperCaseSelector
            }
            
            return [
                UIFontFeatureTypeIdentifierKey: kUpperCaseType,
                UIFontFeatureSelectorIdentifierKey: selector
            ]
        }
        
        private var lowerCaseFeature: [NSObject: AnyObject] {
            var selector: Int
            switch self {
            case .Regular: selector = kDefaultLowerCaseSelector
            case .AllCaps, .SmallCaps: selector = kUpperCaseSmallCapsSelector
            case .PetiteCaps: selector = kUpperCasePetiteCapsSelector
            }
            
            return [
                UIFontFeatureTypeIdentifierKey: kLowerCaseType,
                UIFontFeatureSelectorIdentifierKey: selector
            ]
        }
        
    }
    
    convenience init(preferredTextStyle: String, familyOverride family: String, transform: LetterTransform) {
        let origDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(preferredTextStyle)
        var attributes = origDescriptor.fontAttributes()
        attributes.removeValueForKey(UIFontDescriptorTextStyleAttribute)
        
        // simple "as" conversion won't work, CoreText isn't audited
        let newDesc = CTFontDescriptorCreateCopyWithFamily(origDescriptor, family)
        let newAttributes = newDesc.flatMap({ CTFontDescriptorCopyAttributes($0) as? [String: AnyObject] }) ?? origDescriptor.fontDescriptorWithFamily(family).fontAttributes()
        for (key, value) in newAttributes {
            attributes.updateValue(value, forKey: key)
        }
        
        var features = [ transform.upperCaseFeature, transform.lowerCaseFeature ]
        if let existingFeatures = attributes[UIFontDescriptorFeatureSettingsAttribute] as? [[NSObject: AnyObject]] {
            features += existingFeatures.filter { dict in
                if let type = dict[UIFontFeatureTypeIdentifierKey] as? Int {
                    return type != kUpperCaseType && type != kLowerCaseType
                }
                return true
            }
        }
        attributes[UIFontDescriptorFeatureSettingsAttribute] = features
        
        self.init(fontAttributes: attributes)
    }
    
}
