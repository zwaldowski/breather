//
//  CircleView.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class CircleView: UIView {
    
    override class func requiresConstraintBasedLayout() -> Bool {
        return true
    }
    
    func commonInit() {
        addConstraint(NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Height, multiplier: 1, constant: 0))
        
        autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        clipsToBounds = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        
        if borderColor == nil {
            layer.borderColor = tintColor.CGColor
        }
    }
    
    var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor.map { $0.CGColor } ?? tintColor.CGColor
        }
    }
    
    override func alignmentRectInsets() -> UIEdgeInsets {
        return UIEdgeInsets(top: borderWidth, left: borderWidth, bottom: borderWidth, right: borderWidth)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radius: (CGFloat -> CGFloat) = { ($0 - 1) / 2 }
        layer.cornerRadius = fmin(radius(bounds.width), radius(bounds.height))
    }

}
