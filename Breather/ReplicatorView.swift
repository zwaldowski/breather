//
//  ReplicatorView.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class ReplicatorView: UIView {
    
    // MARK: Init
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: UIView
    
    override class func layerClass() -> AnyClass {
        return CAReplicatorLayer.self
    }
    
    private var replicatorLayer: CAReplicatorLayer {
        return layer as! CAReplicatorLayer
    }
    
    // MARK: CAReplicatorLayer
    
    var instanceCount: Int {
        get { return replicatorLayer.instanceCount }
        set { replicatorLayer.instanceCount = newValue }
    }
    
    var instanceTransform: CGAffineTransform! {
        get { return replicatorLayer.instanceTransform.affineTransform }
        set { replicatorLayer.instanceTransform = CATransform3D(affineTransform: newValue) }
    }
    
}
