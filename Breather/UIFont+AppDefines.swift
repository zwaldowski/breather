//
//  UIFont+AppDefines.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

enum TextStyle {
    case Headline
    case Body
    case Subheadline
    case Footnote
    case Caption1
    case Caption2
    
    init?(font: UIFont) {
        let styles = [ UIFontTextStyleHeadline, UIFontTextStyleBody, UIFontTextStyleSubheadline, UIFontTextStyleFootnote, UIFontTextStyleCaption1, UIFontTextStyleCaption2 ]
        
        var nativeStyle: String?
        for style in styles {
            if font == UIFont.preferredFontForTextStyle(style) {
                nativeStyle = style
                break
            }
        }
        
        if let style = nativeStyle {
            self.init(string: style)
        } else {
            return nil
        }
    }
    
    private init!(string: String) {
        switch string {
        case UIFontTextStyleHeadline: self = .Headline
        case UIFontTextStyleBody: self = .Body
        case UIFontTextStyleSubheadline: self = .Subheadline
        case UIFontTextStyleFootnote: self = .Footnote
        case UIFontTextStyleCaption1: self = .Caption1
        case UIFontTextStyleCaption2: self = .Caption2
        default: return nil
        }
    }
    
    private var stringValue: String {
        switch self {
        case .Headline: return UIFontTextStyleHeadline
        case .Body: return UIFontTextStyleBody
        case .Subheadline: return UIFontTextStyleSubheadline
        case .Footnote: return UIFontTextStyleFootnote
        case .Caption1: return UIFontTextStyleCaption1
        case .Caption2: return UIFontTextStyleCaption2
        }
    }
    
    private var letterTransformValue: UIFontDescriptor.LetterTransform {
        switch self {
        case .Subheadline, .Caption1: return .AllCaps
        default: return .Regular
        }
    }
    
}

extension UIFont {
    
    convenience init(appTextStyle textStyle: TextStyle, size: CGFloat = 0) {
        let descriptor = UIFontDescriptor(preferredTextStyle: textStyle.stringValue, familyOverride: "Avenir Next", transform: textStyle.letterTransformValue)
        self.init(descriptor: descriptor, size: size)
    }
    
}
