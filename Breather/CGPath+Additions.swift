//
//  CGPath+Interpolation.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

extension CGPath {

    var boundingBox: CGRect? {
        let rect = CGPathGetPathBoundingBox(self)
        return CGRectIsNull(rect) ? nil : rect
    }
    
}

/// Three-point difference cubic Hermite represented as Bezier curve
/// http://en.wikipedia.org/wiki/Cubic_Hermite_spline#Representations
func CGPathCreateByInterpolatingPoints<C: CollectionType where C.Index: BidirectionalIndexType, C.Generator.Element == CGPoint>(points: C) -> CGPath! {
    let path = CGPathCreateMutable()
    guard !points.isEmpty else { return path }

    let min = points.startIndex
    let max = points.endIndex.predecessor()
    for i in points.indices {
        let previ = i.advancedBy(-1, limit: min)
        let nexti = i.advancedBy(1, limit: max)
        let nextnexti = nexti.advancedBy(1, limit: max)
        
        let p0 = points[i]
        let p1 = points[nexti]
        let b0 = points[previ]
        let n0 = points[nextnexti]
        
        let m1 = (p1 - p0) * 0.5 + (p0 - b0) * 0.5
        let m2 = (n0 - p1) * 0.5 + (p1 - p0) * 0.5
        let (cp1, cp2) = (p0 + m1 / 3.0, p1 - m2 / 3.0)
        
        if i == min {
            CGPathMoveToPoint(path, nil, p0.x, p0.y)
        }
        
        CGPathAddCurveToPoint(path, nil, cp1.x, cp1.y, cp2.x, cp2.y, p1.x, p1.y)
    }

    return path
}
