//
//  CALayer+PauseResume.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import QuartzCore

// From https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CoreAnimation_guide/AdvancedAnimationTricks/AdvancedAnimationTricks.html
extension CALayer {
    
    func pause() {
        let pausedTime = convertTime(CACurrentMediaTime(), fromLayer: nil)
        speed = 0
        timeOffset = pausedTime
    }
    
    func resume() {
        let pausedTime = timeOffset
        speed = 1
        timeOffset = 0
        beginTime = 0
        let timeSincePause = convertTime(CACurrentMediaTime(), fromLayer: nil) - pausedTime
        beginTime = timeSincePause
    }
    
    func currentTime(offset offset: CFTimeInterval = 0) -> CFTimeInterval {
        return convertTime(CACurrentMediaTime(), fromLayer: nil) + offset
    }

}
