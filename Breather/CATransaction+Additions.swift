//
//  CATransaction+Additions.swift
//  Breather
//
//  Created by Zachary Waldowski on 12/4/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import QuartzCore

extension CATransaction {
    
    class func commit(duration: NSTimeInterval? = nil, timingFunction: CAMediaTimingFunction? = nil, disableImplicitActions: Bool = false, actions: () -> (), completion: (() -> ())? = nil) {
        CATransaction.begin()
        
        if let duration = duration {
            CATransaction.setAnimationDuration(fmax(fabs(duration), DBL_EPSILON))
        }
        CATransaction.setAnimationTimingFunction(timingFunction)
        CATransaction.setDisableActions(disableImplicitActions)
        CATransaction.setCompletionBlock(completion)
        
        CATransaction.lock()
        actions()
        CATransaction.unlock()
        
        CATransaction.commit()
    }
    
}
