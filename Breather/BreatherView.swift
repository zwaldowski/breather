//
//  BreatherView.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit
import CoreGraphics

@IBDesignable
final class BreatherView: UIView {
    
    private weak var shapeView: ShapeView!
    private weak var replicatorView: ReplicatorView!
    private weak var baubleView: CircleView!
    private weak var baubleWidthConstaint: NSLayoutConstraint!
    
    // MARK: Init
    
    dynamic func commonInit() {
        backgroundColor = UIColor.clearColor()
        
        let replicator = ReplicatorView(frame: bounds)
        addSubview(replicator)
        replicatorView = replicator
        
        let shape = ShapeView(frame: bounds)
        shape.fillColor = UIColor.clearColor()
        shape.lineWidth = lineWidth
        shape.lineCap = .Square
        replicator.addSubview(shape)
        shapeView = shape
        
        let bauble = CircleView(frame: CGRect.zero)
        bauble.translatesAutoresizingMaskIntoConstraints = false
        bauble.borderColor = indicatorBorderColor
        bauble.borderWidth = indicatorBorderWidth
        bauble.backgroundColor = indicatorColor
        addSubview(bauble)
        baubleView = bauble
        
        let width = NSLayoutConstraint(item: bauble, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: indicatorRadius * 2)
        bauble.addConstraint(width)
        baubleWidthConstaint = width
        addConstraint(NSLayoutConstraint(item: bauble, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterXWithinMargins, multiplier: 1, constant: 0))
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: UIView
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if superview == nil {
            stopAnimating()
        }
    }
    
    private var wasAnimating: Bool = false
    override var hidden: Bool {
        didSet {
            if hidden {
                wasAnimating = true
                stopAnimating()
            } else if wasAnimating {
                wasAnimating = false
                startAnimating()
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        replicatorView.center = CGPoint(x: bounds.midX, y: bounds.midY)
        
        #if TARGET_INTERFACE_BUILDER
        let shouldMoveBauble = true
        #else
        let shouldMoveBauble = !animating
        #endif
        if shouldMoveBauble { baubleView.center = CGPoint(x: bounds.midX, y: usableDrawingRect.minY - indicatorBorderWidth + indicatorRadius)  }
        
        let (phase, bauble) = popAnimations()
        let percentComplete = phase.map {
            (self.replicatorView.layer.currentTime() - $0.beginTime) / $0.duration
        } ?? bauble.map {
            (self.baubleView.layer.currentTime() - $0.beginTime) / $0.duration
        } ?? 0.0
        updateShape()
        pushAnimations(phase: phase, bauble: bauble, percentComplete: percentComplete)
    }
    
    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        
        setNeedsLayout()
        updateFadesHorizontalEdges(animated: true)
    }
    
    // MARK: Properties
    
    @IBInspectable var velocity: Double = 30 {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable var risingInterval: Double = 3 {
        didSet { setNeedsUpdateShape() }
    }
    
    @IBInspectable var fallingInterval: Double = 3 {
        didSet { setNeedsUpdateShape() }
    }
    
    @IBInspectable var lineWidth: CGFloat = 4 {
        didSet { shapeView.lineWidth = lineWidth }
    }
    
    @IBInspectable var indicatorRadius: CGFloat = 5 {
        didSet { baubleWidthConstaint.constant = indicatorRadius * 2 }
    }
    
    @IBInspectable var indicatorBorderWidth: CGFloat = 2 {
        didSet { baubleView.borderWidth = indicatorBorderWidth }
    }
    
    @IBInspectable var indicatorBorderColor: UIColor? = nil {
        didSet { baubleView.borderColor = indicatorBorderColor }
    }
    
    @IBInspectable var indicatorColor: UIColor? = UIColor.whiteColor().colorWithAlphaComponent(0.5) {
        didSet { baubleView.backgroundColor = indicatorColor }
    }
    
    // MARK: Animation
    
    var animating: Bool {
        return phaseAnimation != nil
    }
    
    func startAnimating() {
        layer.resume()
        
        var phaseShift = phaseAnimation
        if phaseShift == nil {
            phaseShift = CABasicAnimation(keyPath: BreatherView.PhaseAnimationKeyPath)
            phaseShift!.repeatCount = Float.infinity
        } else {
            phaseShift = nil
        }

        var bauble = baubleAnimation
        if bauble == nil {
            bauble = CAKeyframeAnimation(keyPath: BreatherView.BaubleAnimationKeyPath)
            bauble!.repeatCount = Float.infinity
            bauble!.calculationMode = kCAAnimationCubic
        } else {
            bauble = nil
        }
        
        pushAnimations(phase: phaseShift, bauble: bauble)
    }
    
    func stopAnimating() {
        layer.pause()
    }
    
    // MARK: Animation Editing
    
    private class var PhaseAnimation: String { return "phaseAnimation" }
    private class var PhaseAnimationKeyPath: String { return "transform.translation.x" }
    private var phaseAnimation: CABasicAnimation? {
        return replicatorView.layer.animationForKey(BreatherView.PhaseAnimation) as? CABasicAnimation
    }
    
    private class var BaubleAnimation: String { return "baubleAnimation" }
    private class var BaubleAnimationKeyPath: String { return "position.y" }
    private var baubleAnimation: CAKeyframeAnimation? {
        return baubleView.layer.animationForKey(BreatherView.BaubleAnimation) as? CAKeyframeAnimation
    }
    
    private func popAnimations() -> (phase: CABasicAnimation?, bauble: CAKeyframeAnimation?) {
        let phase = phaseAnimation.flatMap { $0.copy() as? CABasicAnimation }
        replicatorView.layer.removeAnimationForKey(BreatherView.PhaseAnimation)

        let bauble = baubleAnimation.flatMap { $0.copy() as? CAKeyframeAnimation }
        baubleView.layer.removeAnimationForKey(BreatherView.BaubleAnimation)

        return (phase, bauble)
    }
    
    private func pushAnimations(phase phase: CABasicAnimation?, bauble: CAKeyframeAnimation?, percentComplete: Double = 0) {
        let duration = CFTimeInterval(risingInterval + fallingInterval)
        let beginTimeOffset = -percentComplete * duration

        if let phase = phase {
            let maxX = shapeView.path.flatMap { $0.boundingBox?.maxX } ?? 0
            
            phase.fromValue = phase.keyPath.flatMap { replicatorView.layer.modelLayer().valueForKeyPath($0) }
            phase.byValue = -1 * Double(maxX)
            phase.beginTime = replicatorView.layer.currentTime(offset: beginTimeOffset)
            phase.duration = duration
            replicatorView.layer.addAnimation(phase, forKey: BreatherView.PhaseAnimation)
        }
        
        if let bauble = bauble {
            let info = shapeInfo
            let rect = usableDrawingRect

            bauble.values = info.unitInterY.map { y -> NSNumber in
                return rect.minY + (y * rect.height)
            }
            bauble.beginTime = baubleView.layer.currentTime(offset: beginTimeOffset)
            bauble.duration = duration
            baubleView.layer.addAnimation(bauble, forKey: BreatherView.BaubleAnimation)
        }
    }
    
    // MARK: Shape Updating
    
    private var usableDrawingRect: CGRect {
        var maxRect = bounds
        maxRect.inset(alignmentRectInsets())
        maxRect.insetInPlace(dx: 0, dy: indicatorRadius + indicatorBorderWidth)
        return maxRect
    }
    
    private struct ShapeInfo {
        let size: CGSize
        let unitPath: CGPath
        let unitInterY: [CGFloat]
    }

    private var shapeInfoCached: ShapeInfo?
    private var shapeInfo: ShapeInfo {
        let size = bounds.size

        if let cached = shapeInfoCached {
            if cached.size == size {
                return cached
            }
        }

        func wavePoints(amplitude A: Double = 1, interval w: Double, isRising rising: Bool, offset: Double = 0) -> (points: [CGPoint], offset: Double) {
            let omega = M_PI / w
            let phi = rising ? 3 * M_PI_2 : M_PI_2
            let increment = 0.1
            let maxX = w + increment

            return (points: 0.stride(to: maxX, by: increment).map {
                CGPoint(x: CGFloat(offset + $0), y: CGFloat(A + (A * sin((omega * $0) + phi))))
            }, offset: maxX)
        }
        
        let (rising, fallingOffset) = wavePoints(amplitude: 0.5, interval: risingInterval, isRising: true)
        let (falling, _) = wavePoints(amplitude: 0.5, interval: fallingInterval, isRising: false, offset: fallingOffset)

        let path = CGPathCreateByInterpolatingPoints([ rising, falling ].lazy.flatten())
        let bauble = [ falling, rising ].flatten().map { (1 - $0.y) }

        let ret = ShapeInfo(size: size, unitPath: path, unitInterY: bauble)
        shapeInfoCached = ret
        return ret
    }
    
    private func setNeedsUpdateShape() {
        shapeInfoCached = nil
        setNeedsLayout()
    }
    
    func updateShape() {
        let info = shapeInfo

        let sx = CGFloat(velocity), sy = usableDrawingRect.height
        var transform = CGAffineTransform(scale: sx, sy)

        guard let path = CGPathCreateCopyByTransformingPath(info.unitPath, &transform),
            scaledRect = path.boundingBox else { return }
        
        let single = scaledRect.width
        let count = ceil((bounds.width + (3.0 * single)) / single)
        let centerOffset = sx * CGFloat(((risingInterval + fallingInterval) / 2.0) - risingInterval)
        let midX = single * floor((count - 1) / 2)
        
        CATransaction.commit(actions: {
            self.replicatorView.instanceCount = Int(count)
            self.replicatorView.instanceTransform = CGAffineTransform(translation: single, 0)
            self.replicatorView.bounds = scaledRect
            self.replicatorView.transform = CGAffineTransform(translation: centerOffset - midX, 0)
            
            self.shapeView.path = path
            self.shapeView.frame = self.replicatorView.bounds
        })
    }
    
    // MARK: Faded Edges
    
    private var internalFadesHorizontalEdges: Bool = false
    @IBInspectable var fadesHorizontalEdges: Bool {
        get { return internalFadesHorizontalEdges }
        set {
            internalFadesHorizontalEdges = newValue
            updateFadesHorizontalEdges()
        }
    }
    
    func setFadesHorizontalEdges(fades: Bool, animated: Bool = false) {
        internalFadesHorizontalEdges = fades
        updateFadesHorizontalEdges(animated: animated)
    }
    
    func updateFadesHorizontalEdges(animated animated: Bool = false) {
        let leading: FadedMaskEdge? = internalFadesHorizontalEdges ? (0, layoutMargins.left) : nil
        let trailing: FadedMaskEdge? = internalFadesHorizontalEdges ? (0, layoutMargins.right) : nil
        configureFadedMask(leading: leading, trailing: trailing, animated: animated)
    }
    
} 