//
//  OverrideRootViewController.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class OverrideRootViewController: UIViewController {
    
    // MARK: UIViewController
    
    override func overrideTraitCollectionForChildViewController(childViewController: UIViewController) -> UITraitCollection? {
        let width = currentBounds(childViewController).width
        let horizontalSizeClass = overrideSizeClass(forWidth: width)

        var collections = [ UITraitCollection(horizontalSizeClass: horizontalSizeClass) ]
        if let orig = super.overrideTraitCollectionForChildViewController(childViewController) {
            collections.insert(orig, atIndex: collections.startIndex)
        }
        return UITraitCollection(traitsFromCollections: collections)
    }
    
    // MARK: Internal

    private func currentBounds(childViewController: UIViewController) -> CGRect {
        let window: UIWindow? = {
            if childViewController.isViewLoaded() {
                return childViewController.view.window
            } else if self.isViewLoaded() {
                return self.view.window
            } else if let delegate = UIApplication.sharedApplication().delegate {
                return delegate.window??.`self`()
            }
            return nil
        }()
        
        let bounds = window?.bounds ?? UIScreen.mainScreen().bounds
        return bounds
    }
    
    private func overrideSizeClass(forWidth width: CGFloat) -> UIUserInterfaceSizeClass {
        return width > 666 ? .Regular : .Compact
    }
    
}
