//
//  UIView+FadedEdges
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

typealias FadedMaskEdge = (offset: CGFloat, thickness: CGFloat)

private class FadedEdgesView: UIView {
    
    // MARK: Init
    
    dynamic func commonInit() {
        autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        backgroundColor = UIColor.clearColor()
        clipsToBounds = true
        
        updateGradientColors()
        updateGradientPoints()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: UIView
    
    override class func layerClass() -> AnyClass {
        return CAGradientLayer.self
    }
    
    private var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    override func layoutSubviews() {
        let maxY = bounds.maxY
        let stop0 = fmin(leading.offset + leading.thickness, leading.offset)
        let stop1 = stop0 + fabs(leading.thickness)
        let stop3 = maxY - fmin(trailing.offset + trailing.thickness, trailing.offset)
        let stop2 = stop3 - fabs(trailing.thickness)
        gradientLayer.locations = [ stop0 / maxY, stop1 / maxY, stop2 / maxY, stop3 / maxY ]
    }
    
    // MARK: Properties
    
    var edgeColor: UIColor = UIColor.clearColor() {
        didSet { updateGradientColors() }
    }
    
    var fillColor: UIColor = UIColor.blackColor() {
        didSet { updateGradientColors() }
    }
    
    var leading: FadedMaskEdge = (0, 0)
    var trailing: FadedMaskEdge = (0, 0)
    var vertical: Bool = false {
        didSet { updateGradientPoints() }
    }
    
    func setFadedMask(leading leading: FadedMaskEdge, trailing: FadedMaskEdge, vertical: Bool, animated: Bool, completion: (() -> ())? = nil) {
        self.leading = leading
        self.trailing = trailing
        setNeedsLayout()
        
        CATransaction.commit(disableImplicitActions: !animated, actions: {
            self.vertical = vertical
            if animated {
                self.layoutIfNeeded()
            }
        }, completion: completion)
    }
    
    // MARK: CALayerDelegate
    
    override func actionForLayer(layer: CALayer, forKey event: String) -> CAAction? {
        if layer == self.layer && (event == "locations" || event == "startPoint" || event == "endPoint") {
            if let transition = super.actionForLayer(layer, forKey: "backgroundColor") as? CAAnimation {
                return CABasicAnimation(action: transition, forLayer: layer, key: event)
            }
        }
        return super.actionForLayer(layer, forKey: event)
    }
    
    // MARK: Internal
    
    private func updateGradientColors() {
        gradientLayer.colors = [
            edgeColor,
            fillColor, fillColor,
            edgeColor
            ].map { $0.CGColor }
    }
    
    private func updateGradientPoints() {
        if vertical {
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        } else {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        }
    }
    
}

extension UIView {
    
    func configureFadedMask(leading startLeading: FadedMaskEdge?, trailing startTrailing: FadedMaskEdge?, vertical: Bool = false, animated: Bool = false) {
        let leading = startLeading ?? (0, 0)
        let trailing = startTrailing ?? (0, 0)
        let maskEnabled = leading.thickness > 0 && trailing.thickness > 0
        
        var view = maskView as? FadedEdgesView
        if view == nil && !maskEnabled { return }
        var completion: (() -> ())?
        
        if maskEnabled && view == nil {
            view = FadedEdgesView(frame: self.bounds)
        } else if !maskEnabled {
            if animated {
                completion = {
                    self.maskView = nil
                }
            } else {
                view = nil
            }
        }
        
        maskView = view
        view?.setFadedMask(leading: leading, trailing: trailing, vertical: vertical, animated: animated, completion: completion)
    }
    
}
