//
//  DynamicTypeLabel.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class DynamicTypeLabel : UILabel {
    private var textStyle: TextStyle? = nil
    
    dynamic func commonInit() {
        textStyle = TextStyle(font: font)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("noteDynamicTypeSettingChanged"), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
        if window != nil {
            noteDynamicTypeSettingChanged()
        }
    }
    
    @objc func noteDynamicTypeSettingChanged() { // UIContentSizeCategoryDidChangeNotification
        if let textStyle = textStyle {
            font = UIFont(appTextStyle: textStyle)
        }
    }
    
}
