//
//  ViewController.swift
//  Breather
//
//  Created by Zachary Waldowski on 11/27/14.
//  Copyright (c) 2014 Zachary Waldowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private var risingLengthSlider: UISlider!
    @IBOutlet private var risingLengthLabel: UILabel!
    @IBOutlet private var fallingLengthSlider: UISlider!
    @IBOutlet private var fallingLengthLabel: UILabel!
    @IBOutlet private var helpLabel: UILabel!
    @IBOutlet private var breatherView: BreatherView!
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateLabel(fromSlider: risingLengthSlider)
        updateLabel(fromSlider: fallingLengthSlider)
        updateForTraitCollection(animated: false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        breatherView.startAnimating()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        breatherView.stopAnimating()
    }
    
    // MARK: Size Classes
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        updateForTraitCollection()
    }
    
    override func willTransitionToTraitCollection(newCollection: UITraitCollection, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransitionToTraitCollection(newCollection, withTransitionCoordinator: coordinator)
        
        coordinator.animateAlongsideTransition({ _ in
            self.updateForTraitCollection(animated: true)
        }, completion: nil)
    }
    
    private func updateForTraitCollection(animated animated: Bool = false) {
        breatherView.setFadesHorizontalEdges(traitCollection.horizontalSizeClass != .Compact, animated: animated)
    }

    // MARK: Formatting
    
    private lazy var secondsFormatter: NSDateComponentsFormatter = {
        let formatter = NSDateComponentsFormatter()
        formatter.allowedUnits = .Second
        formatter.unitsStyle = .Abbreviated
        formatter.formattingContext = .Standalone
        return formatter
    }()
    
    private func stringForTimeInterval(seconds: Float) -> String? {
        let components = NSDateComponents()
        let secondsInt = floor(seconds)
        let leftover = seconds - secondsInt
        components.second = Int(secondsInt)
        components.nanosecond = Int(leftover * Float(NSEC_PER_SEC))
        return secondsFormatter.stringFromDateComponents(components)
    }
    
    // MARK: Actions
    
    @IBAction func updateLabel(fromSlider slider: UISlider) {
        let seconds = Double(slider.value)
        let text = stringForTimeInterval(slider.value)
        if slider == risingLengthSlider {
            risingLengthLabel.text = text
            breatherView.risingInterval = seconds
        } else if slider == fallingLengthSlider {
            fallingLengthLabel.text = text
            breatherView.fallingInterval = seconds
        } else {
            preconditionFailure("Unchecked slider")
        }
    }

    @IBAction func onSliderTouchDown(sender: AnyObject) {
        breatherView.stopAnimating()
    }
    
    @IBAction func onSliderTouchUp(sender: AnyObject) {
        breatherView.startAnimating()
    }
}

